<?php


namespace AppBundle\Block\Service;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\BlockBundle\Block\BaseBlockService;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Sonata\BlockBundle\Model\BlockInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityManager;

class CounterBlockService extends BaseBlockService
{
    protected $em;

    public function __construct(EntityManager $em, $type, $templating)
    {
        $this->type = $type;
        $this->templating = $templating;
        $this->em = $em;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'Entity Counter Blocks';
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultSettings(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'entity'      => 'Add Entity',
            'title'    => 'Insert block Title',
            'class'    => 'bg-blue',
            'icon'    => 'fa-users',
            'template' => 'AppBundle:Block:block_core_counter.html.twig',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function buildEditForm(FormMapper $formMapper, BlockInterface $block)
    {
        $formMapper->add('settings', 'sonata_type_immutable_array', array(
            'keys' => array(
                array('entity', 'url', array('required' => false)),
                array('title', 'text', array('required' => false)),
                array('class', 'text', array('required' => false)),
                array('icon', 'text', array('required' => false)),
            ),
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function validateBlock(ErrorElement $errorElement, BlockInterface $block)
    {
        $errorElement
            ->with('settings[entity]')
                ->assertNotNull(array())
                ->assertNotBlank()
            ->end()
            ->with('settings[title]')
                ->assertNotNull(array())
                ->assertNotBlank()
                ->assertMaxLength(array('limit' => 50))
            ->end()
            ->with('settings[class]')
                ->assertNotNull(array())
                ->assertNotBlank()
            ->end()
            ->with('settings[icon]')
                ->assertNotNull(array())
                ->assertNotBlank()
            ->end();
    }

    /**
     * {@inheritdoc}
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        $settings = $blockContext->getSettings();
          
        $rows = $this->em->getRepository($settings['entity'])->findAll();
        $url = explode("\\",$settings['entity']);
        
        return $this->templating->renderResponse($blockContext->getTemplate(), array(
            'count'     => count($rows),
            'url' => end($url),
            'block'     => $blockContext->getBlock(),
            'settings'  => $settings,
        ), $response);
    }
}
