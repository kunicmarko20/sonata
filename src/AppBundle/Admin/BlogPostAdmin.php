<?php

// src/AppBundle/Admin/CategoryAdmin.php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
        
class BlogPostAdmin extends Admin
{
    public $supportsPreviewMode = true;
    
    public function toString($object)
    {
        return $object->getTitle() != null ? $object->getTitle() : 'Blog Post';
    }
    protected function configureFormFields(FormMapper $formMapper)
    {
          $formMapper
        ->with('Content', array('description' => 'This section contains general content for the web page'))
            ->add('title', 'text', array('help'=>'Set the title of a web page'))
            ->add('body', 'textarea')
            ->add('public','checkbox', array('required' => false))
        ->end()

        ->with('Meta data')
            ->add('category', 'sonata_type_model', array(
                'class' => 'AppBundle\Entity\Category',
                'property' => 'name',
            ))
        ->end()
    ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
            ->add('category', null, array(), 'entity', array(
                'class'    => 'AppBundle\Entity\Category',
                'property' => 'name',
            ))
            
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title')
            ->add('category.name')
            ->add('public', 'boolean', ["editable" => true])
            ->add('_action', 'actions', array(
            'actions' => array(
                'show' => array(),
                'edit' => array(),
                'delete' => array(),
                'Clone' => array(
                    'template' => 'AppBundle:CRUD:list__action_clone.html.twig'
                ),
                'CrDummy' => array(
                    'template' => 'AppBundle:CRUD:list__action_create_dummy.html.twig'
                )
            )
        ))
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('Title')
            ->add('Body')
            ->add('public','boolean')
        ;

    }
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('get', $this->getRouterIdParameter().'/authenticate');
    }
}
