<?php

namespace AppBundle\Controller;


use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Sonata\UserBundle\Entity\BaseUser;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use AppBundle\Entity\BlogPost;

class CRUDController extends Controller
{
    
    public function getAction(Request $request,$id){
        $item = new BaseUser();
        $form = $this->createFormBuilder($item)
                        ->add('Password', PasswordType::class, array('attr'=>array('class'=>'form-control')))
                        ->add('save', SubmitType::class, array('label' => 'Confirm', 'attr'=>array('class'=>'center-block btn btn-primary')))
                        ->getForm();
        
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {  
            $this->checkAccess($item->getPassword(),$id,$request->get('name'));
            return new RedirectResponse($this->admin->generateUrl('list'));
        }
        return $this->render('AppBundle:CRUD:auth.html.twig', array(
            'form' => $form->createView()
        ));
    }
    
    private function switchAction($id,$name){
        switch($name){
                case 'clone':
                    $this->cloneAction($id);
                    break;
                case 'crdummy':
                    $this->createDummyAction();
                    break;
                default : 
                    $this->addFlash('sonata_flash_error', 'You don\'t  have access to do that.');
                    break;   
        }   
    }
    private function checkAccess($password,$id,$name){
        $user = $this->getUser();
        $encoder = $this->get('security.encoder_factory')->getEncoder($user);
        if($encoder->isPasswordValid($user->getPassword(), $password, $user->getSalt()) && $this->admin->isGranted('EDIT') && $this->admin->isGranted('DELETE')){
           $this->switchAction($id,$name); 
        }else {
           $this->addFlash('sonata_flash_error', 'You don\'t  have access to do that.');
        }
    }
    
    private function cloneAction($id)
    {
        $object = $this->admin->getSubject();
        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        $clonedObject = clone $object;
        $clonedObject->setTitle($object->getTitle()." (Clone)");
        $clonedObject->setBody($object->getBody());
        $clonedObject->setDraft($object->getDraft());

        $this->admin->create($clonedObject);

        $this->addFlash('sonata_flash_success', 'Cloned successfully');

    }
    
     private function createDummyAction()
    {

        $Object = new BlogPost();
        $Object->setTitle('Dummy');
        $Object->setBody('this is test');
        $Object->setDraft(false);

        $this->admin->create($Object);

        $this->addFlash('sonata_flash_success', 'Item successfully added');

    }
}
